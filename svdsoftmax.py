"""
SVD-Softmax: Fast Softmax Approximation on Large Vocabulary Neural Networks
http://papers.nips.cc/paper/7130-svd-softmax-fast-softmax-approximation-on-large-vocabulary-neural-networks.pdf
implemented in PyTorch
"""

import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np

dtype = torch.FloatTensor

class SVDSoftmax(nn.Module):
    """svd-approximation-of-softmax class"""

    def __init__(self, tgt_vocab_size, nhid, window_size = 2 ** 5, num_full_view = 2 ** 11):

        """
            initialize SVD softmax
            :param tgt_vocab_size: int, num of vocabulary
            :param nhid: int, num of hidden units
            :param window_size: int, width of preview window W( hidden_units/ 8 is recommended)
            :param num_full_view: int, num of full-view size
            :return: A Tensor [batch_size, seq_length, tgt_vocab_size], output after softmax approximation
        """
        super(SVDSoftmax, self).__init__()
        self.tgt_vocab_size = tgt_vocab_size
        self.nhid = nhid
        self.window_size = window_size
        self.num_full_view = num_full_view

        def normal(tensor, mean=0, std=1):
            """Fills the input Tensor or Variable with values drawn from a normal distribution with the given mean and std
                Args:
                    tensor: a n-dimension torch.Tensor
                    mean: the mean of the normal distribution
                    std: the standard deviation of the normal distribution
                Examples:
                    >>> w = torch.Tensor(3, 5)
                    >>> nninit.normal(w)
                """

            if isinstance(tensor, Variable):
                normal(tensor.data, mean=mean, std=std)
                return tensor
            else:
                return tensor.normal_(mean, std)


        # B=U \Lambda
        # B_SVD
        self.B = Variable(normal(torch.randn(self.tgt_vocab_size, self.nhid).type(dtype),mean=0,std = 1.0 /  \
                    np.sqrt(nhid)), requires_grad = False)

        # transposed V
        self.V_t = Variable(normal(torch.randn(self.nhid, self.nhid).type(dtype),mean=0,std = 1.0 /  \
                    np.sqrt(nhid)), requires_grad = False)

    def update_params(self, weights):
        """
        update svd parameter B, V_t
        :param weights: output weight of softmax
        :return:
        """
        U, _s, V = torch.svd(weights, some=True)

        self.B = torch.matmul(U, torch.diag(_s), out=None)
        self.V_t = torch.transpose(V, 0, 1)

        return

    def get_output(self, dec_output, biases):
        """
        get svd-softmax approximation
        :param dec: A Tensor [batch_size*seq_length, hidden_units], decoder output
        :param biases: A Tensor [tgt_vocab_size], output bias
        :return: A Tensor [batch_size*seq_length, tgt_vocab_size], output after softmax approximation
        """

        _h = torch.matmul(torch.transpose(self.V_t), dec_output)
        _z = torch.add(input = torch.matmul(torch.transpose(self.B[:, :self.window_size]), \
                        _h[:, :self.window_size]), other = biases)
        top_k = torch.topk(_z, k = self.tgt_vocab_size)
        values, _indices = top_k[0], top_k[1]


        _z[_indices] = torch.add(input=torch.matmul(torch.transpose(self.B[_indices, :]), \
                          _h), other=biases[_indices])

        #m = nn.LogSoftmax()
        #log_softmax = m(_z)
        return _z

    def forward(self, input):

        output =

class SVDSoftmaxLoss(nn.Module):

    def __init__(self.tgt_vocab_size, nhid, window_size = 2 ** 5, num_full_view = 2 ** 11):
        super(SVDSoftmaxLoss, self).__init__()
        self.criterion = nn.CrossEntropyLoss()

    def forward(self, input, target):
        bsz = input[0].size(0)

        output = 0.0

        for i in range(len(input)):
            output += self.criterion(input[i], Variable(target[i]))

        output /= bsz

        return output